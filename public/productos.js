//LA API FETCH PROPORCIONA UNA INTERFAZ PARA RECUPERAR RECURSO, INCLUSO A TRAVES DE LA RED
let traeProductos = () => {
    fetch('/bebe', { method: 'GET' })
        .then(response => response.json())
        .then(data => armaTemplate(data));

    function armaTemplate(productos) {
        let template = "";
        productos.forEach(producto => {
            let precioFormateado = new Intl.NumberFormat('es-AR').format(producto.precio);
            template += `<article>`;
            template += `<h4 class = "descripcion">${producto.descripcion}</h4>`;
            template += `<img class = "imagen" src="${producto.imagen}"/>`
            template += `<h5 class = "descripcion">Precio: $${precioFormateado}</h4>`;
            template += `<img class="agregar2" onclick="agregar('${producto.id}','${producto.descripcion}','${producto.imagen}','${producto.precio}')" src="https://cdn-icons-png.flaticon.com/512/117/117885.png">`
            template += `</article>`
        });
        document.querySelector("#productos").innerHTML = template;
    }
}

traeProductos();

//---------------------AGREGAR A CARRITO DESDE PAG PPAL (NO DESDE EL CARRITO)---------------------------------

let contadorArticulosCarrito = 0;
let carrito = [];
let productoPresente;

if (localStorage.getItem("articulos")) {
    carrito = JSON.parse(localStorage.getItem("articulos"));
    console.log(carrito.length);
    //contadorArticulosCarrito = carrito.length;
    //document.querySelector("#contadorArticulosCarrito").innerHTML = contadorArticulosCarrito;
    let cantidadProductosCarrito = 0;
    for (let i = 0; i < carrito.length; i++) {
        let producto = carrito[i];
        
        cantidadProductosCarrito = cantidadProductosCarrito + producto.cantidad;
        console.log(producto.cantidad);
        console.log(cantidadProductosCarrito);
    }
    console.log(cantidadProductosCarrito);
    document.querySelector("#contadorArticulosCarrito").innerHTML = cantidadProductosCarrito;
} else {
    carrito = []
}

function agregar(id, descripcion, imagen, precio) {

    contadorArticulosCarrito += 1
    document.querySelector("#contadorArticulosCarrito").innerHTML = contadorArticulosCarrito;
    console.log("agregando el producto con ID:", id);

    let producto = {
        "id": id,
        "descripcion": descripcion,
        "imagen": imagen,
        "precio": precio,
        "cantidad": 0
    }

    productoPresente = false;

    for (let i = 0; i < carrito.length; i++) {
        if (producto.id === carrito[i].id) {
            productoPresente = true;
            carrito[i].cantidad++;
        }
    }
    if (!productoPresente) {
        producto.cantidad = 1;
        carrito.push(producto);
    }
    localStorage.setItem("articulos", JSON.stringify(carrito));
    console.log("carrito:", carrito)
}

//--------------------------CARRITO-----------------------------------------------------------------------------

function verListado() {
    //mostrarArticulos();
    console.log("estoy en carrito")
    modal.style.opacity = 1;
    modal.style.visibility = "visible";
    armaTemplateCarrito();
    console.log(carrito);
    console.log(carrito.length);
}

let modal = document.querySelector(".contenedor-modal");
let closeModal = document.querySelector(".close");

//CERRAR MODAL CON BOTON CERRAR
closeModal.onclick = function () {
    modal.style.visibility = "hidden";
    modal.style.opacity = 0;
}

let precioTotalProducto;
let precioTotalCarrito;

function armaTemplateCarrito() {
    let precioTotalCarrito = 0;
    let template = '<table><thead><th>Descripcion</th><th>Imagen</th><th>Cantidad</th><th>Precio</th><th>Total producto</th></thead><tbody>';
    for (let i = 0; i < carrito.length; i++) {
        let producto = carrito[i];
        let precioFormateado = new Intl.NumberFormat('es-AR').format(producto.precio);
        precioTotalProducto = producto.precio * producto.cantidad;
        precioTotalCarrito += precioTotalProducto;

        template += `<tr>
                        <td>${producto.descripcion}</td>
                        <td><img src="${producto.imagen}"></td>
                        <td class="cantidad-producto-carrito"><div class="mas-menos" onclick="restarCarrito('${producto.id}')">-</div><div>${producto.cantidad}</div><div class="mas-menos" onclick="sumarCarrito('${producto.id}')">+</div></td>
                        <td>$${precioFormateado}</td>
                        <td id="${producto.id}precioTotalProducto">$${precioTotalProducto}</td>
                    </tr>`
    }
    template += `<th colspan="4" style="text-align= right">Total</th>
                 <th>$${precioTotalCarrito}</th>`
    template += `</tbody></table>`;
    document.querySelector("#productos-carrito").innerHTML = template;

}

function sumarCarrito(id) {
    //precioTotalProducto = Number(precio) * Number(cantidad);
    //document.queryselector(`#{id}precioTotalProducto`).innerHTML = precioTotalProducto; ESTO NO FUNCIONA. NO PUEDE SELECCIONAR ESE ID. PREGUNTAR PORQUE NO SE PUEDE
    console.log(carrito);
    console.log(carrito.length);
    contadorArticulosCarrito = contadorArticulosCarrito + 1;
    document.querySelector("#contadorArticulosCarrito").innerHTML = contadorArticulosCarrito;
    for (let i = 0; i < carrito.length; i++) {
        let producto = carrito[i];
        if (producto.id === id) {
            producto.cantidad++
            armaTemplateCarrito();
        }
    }
    localStorage.setItem("articulos", JSON.stringify(carrito));
}
function restarCarrito(id) {
    console.log("en restar");
    if (contadorArticulosCarrito != 0) {
        contadorArticulosCarrito = contadorArticulosCarrito - 1;
        document.querySelector("#contadorArticulosCarrito").innerHTML = contadorArticulosCarrito;
    }
    //console.log(contadorArticulosCarrito)
    for (let i = 0; i < carrito.length; i++) {
        console.log(carrito);
        console.log(carrito.length);
        let producto = carrito[i];
        if (producto.id === id && producto.cantidad === 1) {
            carrito.splice([i], 1);
            //console.log(i);
            armaTemplateCarrito();
        }
        if (producto.id === id) {
            producto.cantidad-- //NO RESTA!!!!! // SOLUCIONADO. FALTABAN COMILLAS SIMPLES EN EL LLAMADO DE LA FUNCION
            armaTemplateCarrito();
        }
        if (carrito.length === 0) {
            modal.style.visibility = "hidden";
            modal.style.opacity = 0;
        }
    }
    localStorage.setItem("articulos", JSON.stringify(carrito));
}