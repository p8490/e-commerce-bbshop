-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-12-2022 a las 20:20:06
-- Versión del servidor: 10.4.25-MariaDB
-- Versión de PHP: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bebeshop`
--
CREATE DATABASE IF NOT EXISTS `bebeshop` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `bebeshop`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `imagen` varchar(500) NOT NULL,
  `precio` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` VALUES(1, 'Butaca Booster Auto Bebe Megababy 9 A 36 Kg Homologada', 'https://http2.mlstatic.com/D_NQ_NP_774892-MLA31351188877_072019-O.webp', 22000);
INSERT INTO `productos` VALUES(2, 'Butaca O Silla Para Auto De Bebé Mega Baby 0 A 25 Kg', 'https://http2.mlstatic.com/D_NQ_NP_603523-MLA44824423949_022021-O.webp', 34000);
INSERT INTO `productos` VALUES(5, 'Cochecito de paseo doble Joie Evalite dúo coal con chasis color negro', 'https://http2.mlstatic.com/D_NQ_NP_775349-MLA51804939748_102022-O.webp', 95000);
INSERT INTO `productos` VALUES(6, 'Cochecito Con Huevito Cuna Bebé Travel System Mega Baby Ata', 'https://http2.mlstatic.com/D_NQ_NP_842479-MLA52314998085_112022-O.webp', 68000);
INSERT INTO `productos` VALUES(7, 'Practicuna Cuna Megababy Completa Mosquitero+organiz+bolso', 'https://http2.mlstatic.com/D_NQ_NP_805314-MLA49352646272_032022-W.webp', 43800);
INSERT INTO `productos` VALUES(8, 'Practicuna Joie Plegable Commuter Con Cambiador Y 2 Altura', 'https://http2.mlstatic.com/D_NQ_NP_764257-MLA51164802160_082022-O.webp', 58000);
INSERT INTO `productos` VALUES(9, 'Colchón Practicuna Densidad 105x70x8 Arco Iris', 'https://http2.mlstatic.com/D_NQ_NP_930583-MLA47408635727_092021-O.webp', 6900);
INSERT INTO `productos` VALUES(10, 'Mantita Polar Soft Estampado 110 X 75 Cm.', 'https://http2.mlstatic.com/D_NQ_NP_858314-MLA50928191207_072022-O.webp', 3100);
INSERT INTO `productos` VALUES(11, 'Pileta inflable redonda Bestway Coral Kids 51008 de 102cm x 25cm 101L multicolor', 'https://http2.mlstatic.com/D_NQ_NP_793550-MLA41252550280_032020-O.webp', 4100);
INSERT INTO `productos` VALUES(12, 'Pileta inflable cuadrada Bestway 52192 265L multicolor', 'https://http2.mlstatic.com/D_NQ_NP_825941-MLA44961266190_022021-O.webp', 7500);
INSERT INTO `productos` VALUES(13, 'Protector Cubre Colchon Cuna Impermeable Reforzada Creciendo', 'https://http2.mlstatic.com/D_NQ_NP_624311-MLA43823915557_102020-O.webp', 1499);
INSERT INTO `productos` VALUES(14, 'Frazada Cuna Funcional 180x115 Cm Polar Soft Estampada', 'https://http2.mlstatic.com/D_NQ_NP_805090-MLA50806293631_072022-O.webp', 3499);
INSERT INTO `productos` VALUES(15, 'Trapito De Apego Animales Phi Phi Toys', 'https://http2.mlstatic.com/D_NQ_NP_862426-MLA50911658470_072022-O.webp', 1250);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
